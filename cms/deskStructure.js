import S from "@sanity/desk-tool/structure-builder";
import { MdPeople, MdBusiness } from "react-icons/md";
import { FaFile, FaInfoCircle, FaTags, FaShoppingBag } from "react-icons/fa";

export default () =>
  S.list()
    .title("Contenido")
    .items([
      S.listItem()
        .title("Landing")
        .child(
          S.list()
            .title("/landing")
            .items([
              S.listItem()
                .title("Estructura")
                .schemaType("landing")
                .child(
                  S.editor()
                    .id("landing")
                    .schemaType("landing")
                    .documentId("landing")
                ),
              S.divider(),
              S.listItem()
                .title("Servicios")
                .schemaType("service")
                .child(S.documentTypeList("service").title("Servicio"))
                .icon(FaInfoCircle),
              S.listItem()
                .title("Categorías")
                .schemaType("category")
                .child(S.documentTypeList("category").title("Categoría"))
                .icon(FaTags),
              S.listItem()
                .title("Productos")
                .child(S.documentTypeList("product").title("Producto"))
                .icon(FaShoppingBag),
              S.listItem()
                .title("Empresas")
                .schemaType("company")
                .id("companies")
                .child(S.documentTypeList("company").title("Empresas"))
                .icon(MdPeople),
            ])
        ),
      S.listItem()
        .title("Aviso Legal")
        .child(
          S.editor().id("legalPage").schemaType("page").documentId("legal")
        )
        .icon(FaFile),
      S.listItem()
        .title("Política de Privacidad")
        .child(
          S.editor().id("privacyPage").schemaType("page").documentId("privacy")
        )
        .icon(FaFile),
      S.divider(),
      S.listItem()
        .title("Info de empresa")
        .child(
          S.editor()
            .id("myCompany")
            .schemaType("myCompany")
            .documentId("myCompany")
        )
        .icon(MdBusiness),
    ]);
