export default {
  title: "Servicio",
  name: "service",
  type: "document",
  fields: [
    {
      name: "title",
      title: "Título",
      type: "localeString",
    },
    {
      name: "description",
      title: "Descripción",
      type: "localeBlock",
    },
    {
      name: "image",
      title: "Imagen",
      type: "mainImage",
    },
  ],
  preview: {
    select: {
      title: "title",
      image: "image",
    },
    prepare({ title, image }) {
      return {
        title: title.es || "Sin título",
        media: image,
      };
    },
  },
};
