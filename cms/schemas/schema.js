// First, we must import the schema creator
import createSchema from "part:@sanity/base/schema-creator";

// Then import schema types from any plugins that might expose them
import schemaTypes from "all:part:@sanity/base/schema-type";
import mainImage from "./mainImage";
import localeString from "./localeString";
import localeText from "./localeText";
import blockContent from "./blockContent";
import category from "./category";
import company from "./company";
import hero from "./hero";
import product from "./product";
import service from "./service";
import page from "./page";
import landing from "./landing";
import myCompany from "./myCompany";

// Then we give our schema to the builder and provide the result to Sanity
export default createSchema({
  // We name our schema
  name: "default",
  // Then proceed to concatenate our document type
  // to the ones provided by any plugins that are installed
  types: schemaTypes.concat([
    /* Your types here! */
    mainImage,
    localeString,
    localeText,
    blockContent,
    category,
    company,
    hero,
    product,
    service,
    page,
    landing,
    myCompany,
  ]),
});
