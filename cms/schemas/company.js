export default {
  name: "company",
  title: "Empresa",
  type: "document",
  fields: [
    {
      name: "name",
      title: "Nombre",
      type: "string",
    },
    {
      name: "image",
      title: "Imagen",
      type: "mainImage",
    },
    {
      name: "logo",
      title: "Logo",
      type: "image",
    },
  ],
  preview: {
    select: {
      name: "name",
      logo: "logo",
    },
    prepare({ name, logo }) {
      return {
        title: name || "Sin Nombre",
        media: logo,
      };
    },
  },
};
