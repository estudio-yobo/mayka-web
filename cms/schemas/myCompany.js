export default {
  name: "myCompany",
  title: "Datos de Empresa",
  type: "document",
  fields: [
    {
      name: "direction",
      title: "Dirección",
      type: "string",
    },
    {
      name: "mail",
      title: "Email",
      type: "string",
    },
    {
      name: "phone",
      title: "Teléfono",
      type: "string",
    },
  ],
};
