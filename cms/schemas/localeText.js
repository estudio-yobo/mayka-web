const supportedLanguages = [
  { id: "en", title: "English" },
  { id: "es", title: "Spanish", isDefault: true },
];

export default {
  name: "localeBlock",
  type: "object",
  fieldsets: [
    {
      title: "Translations",
      name: "translations",
      options: { collapsible: true },
    },
  ],
  fields: supportedLanguages.map((lang) => ({
    title: lang.title,
    name: lang.id,
    type: "text",
    fieldset: lang.isDefault ? null : "translations",
  })),
};
