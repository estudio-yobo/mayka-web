export default {
  title: "Hero",
  name: "hero",
  type: "document",
  fields: [
    {
      name: "title",
      title: "Título",
      type: "localeString",
    },
    {
      name: "subtitle",
      title: "Subtítulo",
      type: "localeBlock",
    },
    {
      name: "image",
      title: "Imagen",
      type: "image",
    },
    {
      name: "carousel",
      title: "Carrusel de Imágenes",
      type: "array",
      of: [{ type: "image" }],
    },
  ],
};
