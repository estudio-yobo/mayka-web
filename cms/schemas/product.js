export default {
  name: "product",
  title: "Producto",
  type: "document",
  fields: [
    {
      name: "name",
      title: "Nombre",
      type: "localeString",
    },
    {
      name: "image",
      title: "Imagen",
      type: "mainImage",
    },
    {
      name: "category",
      title: "Categoría",
      type: "reference",
      to: { type: "category" },
    },
    {
      name: "subcategory",
      title: "Subategoría",
      type: "reference",
      to: { type: "category" },
    },
  ],
  preview: {
    select: {
      title: "name",
      category: "category.name",
      image: "image",
    },
    prepare({ title, category, image }) {
      return {
        title: title.es || "Sin nombre",
        subtitle: category.es,
        media: image,
      };
    },
  },
};
