export default {
  title: "Landing",
  name: "landing",
  type: "document",
  fields: [
    {
      name: "hero",
      title: "Hero",
      type: "hero",
    },
    {
      name: "services",
      type: "array",
      title: "Servicios",
      of: [
        {
          type: "reference",
          to: { type: "service" },
        },
      ],
    },
    {
      name: "categories",
      type: "array",
      title: "Categorías",
      of: [
        {
          type: "reference",
          to: { type: "category" },
        },
      ],
    },
    {
      name: "clients",
      type: "array",
      title: "Clientes",
      of: [
        {
          type: "reference",
          to: { type: "company" },
        },
      ],
    },
    {
      name: "providers",
      type: "array",
      title: "Proveedores",
      of: [
        {
          type: "reference",
          to: { type: "company" },
        },
      ],
    },
  ],
};
