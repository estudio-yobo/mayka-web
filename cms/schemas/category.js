export default {
  name: "category",
  title: "Categoría",
  type: "document",
  fields: [
    {
      name: "name",
      title: "Nombre",
      type: "localeString",
    },
  ],
  preview: {
    select: {
      name: "name",
    },
    prepare({ name }) {
      console.log(name);

      return { title: name.es || "Untitled" };
    },
  },
};
