require("dotenv").config()
const {
  api: { projectId, dataset },
} = require("../cms/sanity.json")

module.exports = {
  siteMetadata: {
    title: `Cortinas Mayka`,
    description: `Nos encanta hacer que conecte el espacio con la persona, las cortinas y la decoración son algo muy importante. Con nuestro asesoramiento llegarás a la elección correcta y podrás disfrutar de tu espacio. En Mayka somos profesionales, nos avalan 40 años trabajando al servicio de nuestros clientes.`,
    author: `@estudioyobo`,
  },
  plugins: [
    "gatsby-plugin-styled-components",
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    {
      resolve: "gatsby-plugin-google-fonts",
      options: {
        fonts: ["Roboto:200,400,500,600,800"],
      },
    },
    {
      resolve: `gatsby-plugin-google-tagmanager`,
      options: {
        id: "GTM-WTGX63ZW",
      },
    },
    {
      resolve: `gatsby-plugin-google-gtag`,
      options: {
        trackingIds: ["G-WES7WM8Q9C"],
      },
    },
    `gatsby-plugin-image`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: "gatsby-source-sanity",
      options: {
        projectId,
        dataset,
        // To enable preview of drafts, copy .env-example into .env,
        // and add a token with read permissions
        watchMode: true,
        token: process.env.SANITY_TOKEN,
        overlayDrafts: true,
      },
    },
    {
      resolve: "gatsby-plugin-i18n",
      options: {
        langKeyDefault: "es",
        useLangKeyLayout: false,
        prefixDefault: false,
      },
    },
    // {
    //   resolve: "gatsby-plugin-load-script",
    //   options: {
    //     src:
    //       "https://www.google.com/recaptcha/api.js?render=6Ld34KsUAAAAAF7OfaMKULfIkyWOD0yTmviXNd16",
    //   },
    // },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
