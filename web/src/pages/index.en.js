import React from "react"
import { graphql } from "gatsby"
import GraphQLErrorList from "../components/graphql-error-list"
import Hero from "../components/Hero"
import Services from "../components/Services"
import Products from "./../components/Products"
import Caroussel from "../components/Caroussel"
import Header from "../components/Header"
import Contact from "../components/Contact"
import Footer from "../components/Footer"
import SEO from "../components/seo"
import Cookies from "../components/cookies"

import "./styles.css"
import namespace from "../utils/namespace"

export const query = graphql`
  query IndexPageQueryEN {
    landing: sanityLanding(_id: { eq: "landing" }) {
      hero {
        title {
          en
        }
        subtitle {
          en
        }
        image {
          asset {
            _id
            url
          }
        }
        carousel {
          asset {
            gatsbyImageData
          }
        }
      }
      services {
        title {
          en
        }
        description {
          en
        }
        image {
          asset {
            gatsbyImageData
          }
          alt
        }
      }
      categories {
        name {
          en
        }
        id
      }
      clients {
        name
        logo {
          asset {
            _id
            url
          }
        }
        image {
          asset {
            _id
            url
          }
        }
      }
      providers {
        name
        logo {
          asset {
            _id
            url
          }
        }
        image {
          asset {
            _id
            url
          }
        }
      }
    }
    info: sanityMyCompany(_id: { eq: "myCompany" }) {
      direction
      mail
      phone
    }
    products: allSanityProduct {
      nodes {
        name {
          en
        }
        image {
          asset {
            url
          }
        }
        category {
          id
        }
        subcategory {
          name {
            en
            es
          }
        }
      }
    }
  }
`

const IndexPage = ({ data, errors }) => {
  if (errors) {
    return <GraphQLErrorList errors={errors} />
  }
  const t = namespace("en")
  return (
    <main>
      <SEO title="Cortinas Mayka" />
      <Cookies />
      <Header lang="en" logo={data.info.logo} />
      <Hero lang="en" {...data.landing.hero} />
      <Services lang="en" services={data.landing.services} />
      <Products
        lang="en"
        categories={data.landing.categories}
        products={data.products.nodes}
      />
      <Caroussel
        title={t("clients")}
        images={data.landing.clients}
        id="clients"
      />
      <Caroussel title={t("providers")} images={data.landing.providers} />
      <Contact lang="en" {...data.info} />
      <Footer lang="en" {...data.info} />
    </main>
  )
}

export default IndexPage
