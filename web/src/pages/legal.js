import React from "react"
import { graphql } from "gatsby"
import BlockContent from "../components/block-content"
import GraphQLErrorList from "../components/graphql-error-list"
import SEO from "../components/seo"
import Layout from "../components/layout"

export const query = graphql`
  query LegalPageQuery {
    page: sanityPage(_id: { regex: "/(drafts.|)legal/" }) {
      title
      _rawBody
    }
    info: sanityMyCompany(_id: { eq: "myCompany" }) {
      direction
      mail
      phone
    }
  }
`

const LegalPage = props => {
  const { data, errors } = props

  if (errors) {
    return (
      <Layout lang="es" info={data.info}>
        <GraphQLErrorList errors={errors} />
      </Layout>
    )
  }
  return (
    <Layout lang="es" info={data.info}>
      <SEO title={data.page.title} />
      <h1>{data.page.title}</h1>
      <BlockContent blocks={data.page._rawBody || []} />
    </Layout>
  )
}
LegalPage.defaultProps = {
  data: {
    page: {
      title: "No title",
    },
  },
}
export default LegalPage
