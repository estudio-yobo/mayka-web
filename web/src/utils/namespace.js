const translations = {
  es: {
    clients: "Clientes",
    providers: "Proveedores",
    clients_suppliers: "Clientes y Proveedores",
    services: "Servicios",
    service: "Servicio",
    contact: "Contacto",
    products: "Productos",
    name: "Nombre",
    email: "Email",
    phone: "Teléfono",
    send: "Enviar",
    "error-with-status": "Un error {{statusCode}} ocurrió en en servidor",
    "error-without-status": "Un error ha ocurrido en el servidor",
    privacy: "Política de Privacidad",
    legal: "Aviso Legal",
  },

  en: {
    clients: "Clients",
    providers: "Suppliers",
    clients_suppliers: "Clients & Suppliers",
    services: "Services",
    service: "Service",
    products: "Products",
    contact: "Contact",
    name: "Name",
    email: "Email",
    phone: "Telephone",
    send: "Send",
    "error-with-status": "A {{statusCode}} error occurred on server",
    "error-without-status": "An error occurred on the server",
    privacy: "Privacy Policy",
    legal: "Legal",
  },
}

const namespace = lang => key => translations[lang][key]

export default namespace
