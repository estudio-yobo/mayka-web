import React from "react"
import styled from "styled-components"
import Facebook from "./Icons/Facebook"
import Instagram from "./Icons/Instagram"
import namespace from "../utils/namespace"

const FooterStyled = styled.footer`
  background: black;
  color: white;
  padding: 2rem 4rem;
  display: flex;
  & > div {
    flex: 1;
  }
  a {
    color: white;
    font-weight: 900;
    text-decoration: none;
  }
  @media (max-width: 600px) {
    display: none;
  }
`

function Footer({ direction, phone, mail, lang }) {
  const t = namespace(lang)
  const date = new Date()
  const year = date.getFullYear()
  return (
    <FooterStyled>
      <div>
        <div>
          <a href="/privacy">{t("privacy")}</a>
        </div>
        <div>
          <a href="/legal">{t("legal")}</a>
        </div>
      </div>
      <div>
        <div>{direction}</div>
        <div>
          <a href={`mailto:${mail}`}>{mail}</a>
        </div>
        <div>{phone}</div>
      </div>
      <div>
        <a
          href="https://instagram.com/mayka_decoracion"
          target="_blank"
          rel="noopener noreferrer"
        >
          <Instagram />
        </a>
        <a
          href="https://www.facebook.com/CortinasMayka/"
          target="_blank"
          rel="noopener noreferrer"
        >
          <Facebook />
        </a>
      </div>
      <div>
        <div>
          Made with ❤ by{" "}
          <a
            href="http://www.estudioyobo.com"
            target="_blank"
            rel="noopener noreferrer"
          >
            Estudio Yobo
          </a>
        </div>
        <div>©{year} Cortinas Mayka</div>
      </div>
    </FooterStyled>
  )
}

export default Footer
