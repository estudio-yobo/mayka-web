import React from "react"
import styled from "styled-components"
import Header from "./Header"
import Footer from "./Footer"

const Main = styled.main`
  width: 80%;
  margin: calc(80px + 2rem) auto;
`

const Layout = ({ children, lang, info }) => {
  return (
    <>
      <Header lang={lang} redirect />
      <Main>{children}</Main>
      <Footer lang={lang} {...info} />
    </>
  )
}

export default Layout
