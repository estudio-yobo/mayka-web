import React from 'react';
import styled, {keyframes} from "styled-components";

const LoadingWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
`;

const makeBig = keyframes`
0% {
  transform: scale(.9);
}

50% {
  transform: scale(1.1);
}
100% {
  transform: scale(.9);
}
`
const Logo = styled.svg`
width: 20%;
animation: ${makeBig} .9s linear infinite;
`

const SVGLogo = () => (
  <Logo viewBox="0 0 527.24409 377" fill="#825b1c">
    <g>
    <polygon points="233.182 191.661 212.762 191.661 205.252 155.041 201.142 135.041 195.492 107.511 189.532 135.041 185.192 155.041 155.352 292.791 126.682 112.121 110.442 178.961 92.622 94.571 71.242 175.771 6.122 175.771 6.122 155.771 55.832 155.771 91.932 18.621 97.022 18.621 111.812 88.651 128.122 21.551 132.562 21.551 158.392 184.311 193.652 21.551 198.372 21.551 233.182 191.661"/>
    <polygon points="211.327 155.04 180.669 155.04 184.875 135.04 211.327 135.04 211.327 155.04"/>
    <rect x="425.13486" y="128.08345" width="33.41406" height="20"/>
    <polygon points="265.682 206.295 245.942 209.525 217.742 37.125 238.012 37.125 265.682 206.295"/>
    <polygon points="340.752 8.29 207.962 368.71 186.652 368.71 245.742 208.32 259.782 170.21 319.432 8.29 340.752 8.29"/>
    <polygon points="377.142 8.29 347.612 86.64 329.462 134.8 274.542 280.54 253.162 280.54 355.772 8.29 377.142 8.29"/>
    <polygon points="521.122 155.775 521.122 175.775 463.462 175.775 444.692 92.525 391.292 287.575 319.232 109.515 401.182 43.445 413.732 59.015 343.342 115.765 387.622 225.205 443.382 21.555 449.182 21.555 479.462 155.775 521.122 155.775"/>
    </g>
    </Logo>
)

const LoadingScreen = () => {
  return (
    <div className="loader">
    <LoadingWrapper >
      <SVGLogo />
    </LoadingWrapper>
    </div>
  )
}

export default LoadingScreen
