import React, { useState } from "react"
import styled from "styled-components"
import { Script } from "gatsby"
import namespace from "../utils/namespace"

const Button = styled.button`
  color: white;
  background: black;
  width: 33%;
  font-size: 1.4rem;
  border: none;
  padding: 10px;
  cursor: pointer;
  :hover {
    background: #111;
  }
`

const Form = styled.form`
  display: flex;
  flex-direction: column;
  input {
    width: 60%;
    border: none;
    border-bottom: solid 0.6px #333;
    padding: 5px;
    margin: 10px 0;
    @media (max-width: 600px) {
      width: 95%;
    }
  }
  textarea {
    border: solid 1px rgba(0, 0, 0, 0.2);
    padding: 5px;
    box-shadow: 0 0 10px 2px rgba(0, 0, 0, 0.2);
  }
`

const ContactStyled = styled.section`
  margin: 10rem 1rem;
  display: grid;
  grid-template-columns: 1fr 3fr 1fr 3fr 1fr;
  grid-template-areas: "h h h h h" ". f . i ." ". f . i .";
  h1 {
    grid-area: h;
  }
  form {
    grid-area: f;
  }
  a {
    color: black;
    text-decoration: none;
    &:hover {
      text-decoration: underline;
    }
  }
  .info {
    grid-area: i;
    font-size: 1.6rem;
    font-weight: 400;
  }
  @media (max-width: 600px) {
    grid-template-areas: "h h h h h" ". f f f ." ". i i i .";
    .info {
      font-size: 1.4rem;
      font-weight: 400;
    }
  }
`

const Legal = styled.span`
  font-size: 0.6rem;
  margin: 8px 0;
`

const Contact = ({ direction, mail, phone, lang }) => {
  const [status, setStatus] = useState()
  const t = namespace(lang)
  function handleSubmit(e) {
    e.preventDefault()
    if (typeof window !== undefined) {
      // Get form data as object
      const formData = new FormData(e.target)
      const data = {}
      for (let [name, value] of formData.entries()) {
        data[name] = value
      }
      // Validate captcha to send form
      window.grecaptcha.ready(async function () {
        const googleToken = await window.grecaptcha.execute(
          "6Ld34KsUAAAAAF7OfaMKULfIkyWOD0yTmviXNd16",
          {
            action: "contact",
          },
        )
        console.log("googleToken", googleToken)

        fetch("https://forms.estudioyobo.com/mayka", {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            ...data,
            googleToken,
          }),
        })
          .then(res => res.json())
          .then(res => {
            setStatus(res.message)
          })
      })
    }
  }
  return (
    <ContactStyled id="contact">
      <Script src="https://www.google.com/recaptcha/api.js?render=6Ld34KsUAAAAAF7OfaMKULfIkyWOD0yTmviXNd16" />
      <h1>{t("contact")}</h1>
      <Form onSubmit={handleSubmit}>
        <input type="text" name="name" id="name" placeholder={t("name")} />
        <input type="email" name="mail" id="mail" placeholder={t("email")} />
        <input
          type="telephone"
          name="phone"
          id="phone"
          placeholder={t("phone")}
        />
        <textarea name="message" id="" cols="30" rows="10" />
        <Legal>
          This site is protected by reCAPTCHA and the Google
          <a href="https://policies.google.com/privacy"> Privacy Policy</a> and
          <a href="https://policies.google.com/terms"> Terms of Service</a>{" "}
          apply.
        </Legal>
        <Button>{t("send")}</Button>
        <span>{status}</span>
      </Form>
      <div className="info">
        <p>
          <img
            src="/location.svg"
            alt="location icon"
            style={{
              width: 20,
              height: 20,
              marginRight: 5,
            }}
          />
          <a
            href="https://www.google.com/maps/place/Calle+la+Torre,+34,+03204+Elche,+Alicante/@38.2611066,-0.7061773,18.23z"
            target="_blank"
            rel="noopener noreferrer"
          >
            {direction}
          </a>
        </p>
        <p>
          <img
            src="/mail.svg"
            alt="mail icon"
            style={{
              width: 20,
              height: 20,
              marginRight: 5,
            }}
          />
          <a href={`mailto:${mail}`}>{mail}</a>
        </p>
        <p>
          <img
            src="/phone.svg"
            alt="phone icon"
            style={{
              width: 20,
              height: 20,
              marginRight: 5,
            }}
          />
          <a href={`tel:+34${phone.split(" ").join("")}`}>{phone}</a>
        </p>
      </div>
    </ContactStyled>
  )
}

export default Contact
