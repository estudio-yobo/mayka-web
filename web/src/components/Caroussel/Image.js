import React from "react"
import PropTypes from "prop-types"
import styled from "styled-components"

const Img = styled.div`
  cursor: pointer;
  background-image: url('${({ image }) => image}');
  background-color: #d4d4d4;
  background-position: center;
  background-size: cover;
  height: 15rem;
  margin: 0 1rem;
  display: flex;
  justify-content: center;
  text-align: center;
  align-items: center;
  @media (max-width: 600px) {
    height: 10rem;
  }
`

const Title = styled.span`
  background-color: white;
  padding: 1rem;
  width: 40%;

  @media (max-width: 600px) {
    padding: 10px;
    width: 80%;
  }
`

function Image({ image, name, onClick }) {
  return (
    <Img image={image ? image.asset.url : null} onClick={onClick}>
      <Title>{name}</Title>
    </Img>
  )
}

Image.propTypes = {
  image: PropTypes.string,
  title: PropTypes.string,
  onClick: PropTypes.func,
}

export default Image
