import React, { useState } from "react"
import PropTypes from "prop-types"
import Slider from "react-slick"
import Modal from "react-modal"
import styled from "styled-components"
import Image from "./Image"

import "slick-carousel/slick/slick.css"
import "./theme.css"

Modal.setAppElement("#___gatsby")

const CarousselStyled = styled.section`
  height: 50vh;
  padding-top: 12vh;
`

const ModalImg = styled.img`
  width: 100%;
  height: 100%;
  object-fit: contain;
`
const BrandImg = styled.img`
  height: 100px;
  object-fit: contain;
  position: absolute;
  bottom: 0;
  right: 0;
`

const Caroussel = ({ title, images, id }) => {
  const [modal, setModal] = useState({ isOpen: false, img: "" })
  function closeModal() {
    setModal({ isOpen: false, img: "" })
  }

  return (
    <CarousselStyled id={id}>
      <h1>{title}</h1>
      <Slider
        infinite
        autoplay
        autoplaySpeed={2500}
        centerMode
        slidesToShow={3}
        responsive={[
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              rows: 2,
            },
          },
        ]}
        arrows
        dots
      >
        {images.map((props, i) => (
          <Image
            key={i}
            {...props}
            onClick={() => setModal({ isOpen: true, img: props })}
          />
        ))}
      </Slider>
      <Modal
        isOpen={modal.isOpen}
        onRequestClose={closeModal}
        style={{
          overlay: {
            zIndex: 100,
          },
        }}
      >
        <ModalImg
          src={modal.img.image ? modal.img.image.asset.url : null}
          onClick={closeModal}
        />
        <BrandImg
          src={modal.img.logo ? modal.img.logo.asset.url : null}
          onClick={closeModal}
        />
      </Modal>
    </CarousselStyled>
  )
}

Caroussel.propTypes = {
  title: PropTypes.string,
  images: PropTypes.arrayOf(PropTypes.object),
  id: PropTypes.string,
}

export default Caroussel
