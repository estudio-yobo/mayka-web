import React from "react"
import styled from "styled-components"

const StyledProduct = styled.div`
  height: 100%;
  background-image: url(${props => props.image});
  cursor: ${({ clickable }) => (clickable ? "pointer" : "default")};
  background-size: cover;
  background-position: center;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin: 2px;
  flex: 1;
  position: relative;

  &::before {
    content: "";
    background: rgba(255, 255, 255, 0.6);
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    opacity: 0;
    transition: all 0.2s ease-in-out;
  }
  span {
    text-align: center;
    opacity: 0;
    transition: all 0.2s ease-in-out;
    z-index: 1;
    font-size: 12px;
    &:first-child {
      font-weight: bold;
      font-size: 16px;
    }
  }
  &:hover {
    &::before {
      opacity: 1;
    }
    span {
      opacity: 1;
    }
  }
`

const Product = ({ lang, product, onClick }) => {
  return (
    <StyledProduct
      image={
        product.image && product.image.asset ? product.image.asset.url : ""
      }
      onClick={() => onClick(product)}
    >
      <span>{product.name[lang]}</span>
      {product.subcategory && <span>{product.subcategory.name[lang]}</span>}
    </StyledProduct>
  )
}

Product.defaultProps = {
  onClick: () => null,
}

export default Product
