import React, { useEffect } from "react"
import styled from "styled-components"

export const StyledHeader = styled.div`
  background: #e7e7e7;
  padding: 1rem 0.3rem;
  display: flex;
  justify-content: space-around;
  position: sticky;
  top: 95px;
  z-index: 90;
  transition: all 0.2s ease-in-out;
  margin: 0 auto;
  box-sizing: border-box;
  @media (max-width: 870px) {
    top: 0;
  }
`

const Option = styled.span`
  cursor: pointer;
  transition: all 0.3s ease-in-out;
  font-weight: ${({ selected }) => (selected ? "bold" : "normal")};
  font-size: 1.2rem;
  &:hover {
    transform: scale(1.05);
  }
  @media (max-width: 870px) {
    font-size: 1rem;
  }
`

/**
 * Dispatches the `sticky-event` custom event on the target element.
 * @param {boolean} stuck True if `target` is sticky.
 * @param {!Element} target Element to fire the event on.
 */
function fireEvent(stuck, target, intersection) {
  const e = new CustomEvent("sticky-change", {
    detail: { stuck, target, intersection },
  })
  document.dispatchEvent(e)
}

/**
 * @param {!Element} container
 * @param {string} className
 */
function addSentinels(container, className) {
  return Array.from(container.querySelectorAll(".sticky")).map(el => {
    const sentinel = document.createElement("div")
    sentinel.classList.add("sticky_sentinel", className)
    return el.parentElement.appendChild(sentinel)
  })
}

function observeHeaders(container) {
  const observer = new IntersectionObserver(
    records => {
      for (const record of records) {
        const targetInfo = record.boundingClientRect
        const stickyTarget = record.target.parentElement.querySelector(
          ".sticky"
        )
        const rootBoundsInfo = record.rootBounds

        // Started sticking.
        if (targetInfo.bottom < rootBoundsInfo.top) {
          fireEvent(true, stickyTarget, record.intersectionRatio)
        }

        // Stopped sticking.
        if (
          targetInfo.bottom >= rootBoundsInfo.top &&
          targetInfo.bottom < rootBoundsInfo.bottom
        ) {
          fireEvent(false, stickyTarget, record.intersectionRatio)
        }
      }
    },
    {
      threshold: [0, 0.25, 0.5, 0.75, 1],
    }
  )

  // Add the bottom sentinels to each section and attach an observer.
  const sentinels = addSentinels(container, "sticky_sentinel--top")
  sentinels.forEach(el => observer.observe(el))
}

// b - beginning position
// e - ending position
// i - your current value (0-1)
function getTween(b, e, i) {
  return b + i * (e - b)
}

const MenuHeader = ({
  lang,
  container,
  selected,
  onSectionSelected,
  options,
}) => {
  useEffect(() => {
    if (container.current && window.IntersectionObserver) {
      observeHeaders(container.current)
    }
    function stickyChange({ detail: { target, stuck, intersection } }) {
      target.style.width = `${getTween(90, 100, intersection)}%`
    }
    document.addEventListener("sticky-change", stickyChange)
    return () => {
      document.removeEventListener("sticky-change", stickyChange)
    }
  }, [container])
  return (
    <StyledHeader className="sticky">
      {options.map(option => (
        <Option
          key={option.id}
          selected={selected === option.id}
          onClick={() => {
            onSectionSelected(option.id)
            const element = document.getElementById("products")
            if (typeof window !== undefined) {
              window.scrollTo({
                left: 0,
                top: element.offsetTop,
                behavior: "smooth",
              })
            }
          }}
        >
          {option.name[lang]}
        </Option>
      ))}
    </StyledHeader>
  )
}

export default MenuHeader
