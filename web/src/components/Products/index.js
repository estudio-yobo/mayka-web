import React, { useState, useRef } from "react"
import PropTypes from "prop-types"
import styled from "styled-components"
import MenuContent from "./MenuContent"
import MenuHeader from "./MenuHeader"
import namespace from "../../utils/namespace"

const Section = styled.section`
  margin-top: calc(60px + 1rem);
  padding-top: 12vh;
  position: relative;
  /* min-height: calc(100vh - 60px - 1rem); */

  .sticky_sentinel {
    background: yellow;
    position: absolute;
    left: 0;
    right: 0; /* needs dimensions */
    visibility: hidden;
  }
  .sticky_sentinel--top {
    /* Adjust the height and top values based on your on your sticky top position.
  e.g. make the height bigger and adjust the top so observeHeaders()'s
  IntersectionObserver fires as soon as the bottom of the sentinel crosses the
  top of the intersection container. */
    height: 40px;
    top: 0px;
  }
  .sticky_sentinel--bottom {
    bottom: 0;
    /* Height should match the top of the header when it's at the bottom of the
  intersection container. */
    height: calc(60px + 1rem);
  }
`

const Menu = ({ lang, categories, products }) => {
  const [selected, setSelected] = useState(
    categories.length > 0 ? categories[0].id : ""
  )
  const t = namespace(lang)
  const ref = useRef(null)
  const content = products.filter(prod => prod.category.id === selected)
  return (
    <Section id="products" ref={ref}>
      <h1>{t("products")}</h1>
      <MenuHeader
        options={categories}
        selected={selected}
        onSectionSelected={setSelected}
        container={ref}
        lang={lang}
      />
      <MenuContent content={content} lang={lang} />
    </Section>
  )
}

Menu.propTypes = {
  categories: PropTypes.array,
  products: PropTypes.array,
  lang: PropTypes.string,
}

export default Menu
