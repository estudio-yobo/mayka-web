import React, { useState } from "react"
import styled from "styled-components"
import Modal from "react-modal"
import Product from "./Product"

export const StyledContent = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(20rem, 1fr));
  grid-auto-rows: 1fr;

  &::before {
    content: "";
    width: 0;
    padding-bottom: 100%;
    grid-row: 1 / 1;
    grid-column: 1 / 1;
  }

  > *:first-child {
    grid-row: 1 / 1;
    grid-column: 1 / 1;
  }
`

const ModalImg = styled.img`
  width: 100%;
  height: 100%;
  object-fit: contain;
`

const MenuContent = ({ content, lang }) => {
  const [modal, setModal] = useState(null)
  function closeModal() {
    setModal(null)
  }
  return (
    <StyledContent>
      {content.map((option, i) => (
        <Product
          key={option.name + i}
          product={option}
          onClick={product =>
            setModal(product.image && product.image.asset.url)
          }
          lang={lang}
        />
      ))}
      <Modal
        isOpen={Boolean(modal)}
        onRequestClose={closeModal}
        style={{
          overlay: {
            zIndex: 100,
          },
        }}
      >
        <ModalImg src={modal} onClick={closeModal} />
      </Modal>
    </StyledContent>
  )
}

export default MenuContent
