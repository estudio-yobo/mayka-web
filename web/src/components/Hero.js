import React from "react"
import styled from "styled-components"
import Slider from "react-slick"
import { GatsbyImage } from "gatsby-plugin-image"
import namespace from "../utils/namespace"

require("slick-carousel/slick/slick.css")
require("slick-carousel/slick/slick-theme.css")

const Section = styled.section`
  background-image: url(${({ bg }) => bg});
  background-size: cover;
  color: white;
  display: grid;
  height: 100vh;
  width: 100%;
  grid-template-columns: 5% 2fr 1fr 2fr 5%;
  grid-template-rows: repeat(7, 1fr);
  grid-template-areas:
    ". logo logo . ."
    ". logo logo . ."
    ". title title . ."
    ". title title img ."
    ". desc . img ."
    ". desc . img ."
    ". . cta img .";

  .logo {
    grid-area: logo;
  }
  h1 {
    grid-area: title;
    font-size: 6vw;
    line-height: 6vw;
    text-transform: uppercase;
    margin: 0;
  }
  .desc {
    grid-area: desc;
    font-size: 1.4rem;
    font-weight: 200;
  }
  .img {
    grid-area: img;
    box-shadow: 0px 0px 10px 10px rgba(0, 0, 0, 0.3);
    overflow: hidden;
    align-self: flex-end;
    img {
      width: 100%;
      height: 100%;
      object-fit: cover;
    }
  }
  a {
    grid-area: cta;
    color: white;
    background: black;
    margin: 1rem auto 0;
    width: 100%;
    font-size: 1.4rem;
    border: none;
    text-transform: uppercase;
    cursor: pointer;
    text-align: center;
    line-height: 100px;
    text-decoration: none;
    align-self: flex-end;
    transition: all 0.2s ease-out;
    :hover {
      background: #111;
    }
  }
  @media (max-width: 1200px) {
    h1 {
      font-size: 5rem;
      line-height: 5rem;
    }
    .desc {
      font-size: 1.4rem;
    }
  }
  @media (max-width: 800px) {
    grid-template-areas:
      ". logo logo . ."
      ". title title title ."
      ". desc desc desc ."
      ". desc desc desc ."
      ". . img img img"
      ". . img img img"
      ". cta cta cta .";

    h1 {
      font-size: 4rem;
      line-height: 4rem;
    }
    .desc {
      font-size: 1.4rem;
    }
    a {
      width: 60%;
      height: 65px;
      line-height: 65px;
    }
  }
  @media (max-width: 600px) {
    h1 {
      font-size: 3rem;
      line-height: 3rem;
    }
    .desc {
      font-size: 1.1rem;
    }
  }
`

const Hero = ({ title, subtitle, image, carousel, lang }) => {
  const t = namespace(lang)
  return (
    <Section bg={image.asset.url}>
      <h1>{title[lang]}</h1>
      <div className="desc">{subtitle[lang]}</div>
      <Slider
        infinite
        autoplay
        autoplaySpeed={2500}
        adaptiveHeight
        className="img"
        arrows={false}
      >
        {carousel.map(image => (
          <GatsbyImage
            key={image}
            image={image.asset.gatsbyImageData}
            class="gbimg"
          />
        ))}
      </Slider>
      <a href="#contact">{t("contact")}</a>
    </Section>
  )
}

export default Hero
