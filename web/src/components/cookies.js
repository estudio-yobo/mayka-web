import React, { useEffect, useRef } from "react"
import { useLayoutEffect } from "react"
import styled from "styled-components"

const CookiesStyled = styled.div`
  position: fixed;
  bottom: 1rem;
  left: 1rem;
  display: inline-block;
  opacity: 0;
  visibility: hidden;
  transition: transform 0.3s ease-out, opacity 0.3s ease-out,
    visibility 0.3s ease-out;
  transform: translate(0, 50%);
  z-index: 5101;
  background: #825b1c;
  color: white;
  padding: 10px;
  font-size: 14px;

  a {
    color: white;
  }

  button {
    background: #111;
    color: white;
    padding: 0.5rem;
    border: none;
    cursor: pointer;
  }

  &.showed {
    opacity: 1;
    visibility: visible;
    transform: translate(0, 0);
  }
`

const setCookie = () => {
  var name = "cookielaw_module"
  var value = "1"
  var today = new Date()
  var expire = new Date()
  expire.setTime(today.getTime() + 3600000 * 24 * 14)
  document.cookie =
    name +
    "=" +
    value +
    ";path=/;" +
    (expire == null ? "" : "; expires=" + expire.toGMTString())
}
function Cookies() {
  const cookieBanner = useRef(null)
  useEffect(() => {
    console.log(document.cookie.indexOf("cookielaw_module=1"))
    if (
      cookieBanner.current &&
      document.cookie.indexOf("cookielaw_module=1") === -1
    ) {
      cookieBanner.current.classList.add("showed")
    }
  }, [cookieBanner])
  const accept = e => {
    e.preventDefault()
    cookieBanner.current.classList.remove("showed")
    setCookie()
  }
  return (
    <CookiesStyled ref={cookieBanner}>
      <p>
        Al navegar en la web de Cortinas Mayka está aceptando las{" "}
        <a href="/privacy">Políticas de Cookies</a>.<br /> En el caso de no
        querer aceptarlas puede cerrar la página.
      </p>
      <button onClick={accept}>Aceptar</button>
    </CookiesStyled>
  )
}

export default Cookies
