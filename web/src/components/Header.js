import React from "react"
import styled from "styled-components"
import Facebook from "./Icons/Facebook"
import Instagram from "./Icons/Instagram"
import MobileMenu from "./MobileMenu"
import namespace from "../utils/namespace"

const HeaderStyled = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: center;
  position: fixed;
  width: 90%;
  left: 0;
  right: 0;
  top: 1rem;
  background: white;
  padding: 10px;
  margin: 0 auto;
  z-index: 100;
  box-sizing: border-box;
  box-shadow: 0 0 5px 0px rgba(0, 0, 0, 0.2);

  > div {
    display: flex;
  }
  .lang,
  nav {
    align-self: center;
  }
  .lang {
    a {
      margin: 0 5px;
      text-decoration: none;
      color: black;
      &.active {
        font-weight: bold;
        pointer-events: none;
      }
      &:hover {
        font-weight: 900;
      }
    }
  }
  @media (max-width: 870px) {
    justify-content: center;
    position: absolute;
    > div {
      display: none;
    }
  }
  @media (max-width: 400px) {
    padding: 5px;
  }
`

const Logo = styled.img`
  height: 90px;
  margin: -5px 0 -30px;
  width: auto;
  @media (max-width: 400px) {
    height: 60px;
    margin: 0;
  }
  @media (max-height: 615px) {
    margin: -5px 0 -15px;
  }
`

const LinkStyled = styled.a`
  color: black;
  text-decoration: none;
  margin: 0 1rem;

  &:hover {
    font-weight: 900;
  }
`

const Header = ({ redirect, logo, lang }) => {
  const t = namespace(lang)
  return [
    <HeaderStyled key="normalheader">
      <a href="/">
        <Logo src="/logo.svg" alt="Logo" />
      </a>
      <div>
        <nav>
          <LinkStyled href={`${redirect ? "/" : ""}#services`}>
            {t("services")}
          </LinkStyled>
          <LinkStyled href={`${redirect ? "/" : ""}#products`}>
            {t("products")}
          </LinkStyled>
          <LinkStyled href={`${redirect ? "/" : ""}#clients`}>
            {t("clients_suppliers")}
          </LinkStyled>
          <LinkStyled href={`${redirect ? "/" : ""}#contact`}>
            {t("contact")}
          </LinkStyled>
        </nav>
        <div className="social">
          <a
            href="https://instagram.com/mayka_decoracion"
            target="_blank"
            rel="noopener noreferrer"
          >
            <Instagram color="#333" width={30} height={30} />
          </a>
          <a
            href="https://www.facebook.com/CortinasMayka/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <Facebook color="#333" width={30} height={30} />
          </a>
        </div>
        <div className="lang">
          <a className={lang === "es" ? "active" : ""} href="/">
            ES
          </a>
          <a className={lang === "en" ? "active" : ""} href="/en">
            EN
          </a>
        </div>
      </div>
    </HeaderStyled>,
    <MobileMenu
      key="mobileheader"
      logo={logo}
      lang={lang}
      t={t}
      redirect={redirect}
    />,
  ]
}

Header.defaultProps = {
  redirect: false,
}

export default Header
