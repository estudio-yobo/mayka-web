import React, { useState, useRef } from "react"
import styled from "styled-components"
import Facebook from "./Icons/Facebook"
import Instagram from "./Icons/Instagram"
import Back from "./Icons/Back"
import useOnClickOutside from "../utils/hooks/useOnClickOutside"

const Button = styled.div`
  background: white;
  border-radius: 50%;
  width: 60px;
  height: 60px;
  position: fixed;
  bottom: 15px;
  right: 15px;
  box-shadow: 0px 6px 10px -2px rgba(0, 0, 0, 0.3);
  z-index: 200;
  display: none;
  transition: all 0.3s ease-in-out;
  span {
    height: 4px;
    background: #333;
    width: 30px;
    left: 15px;
    position: absolute;
    &:nth-child(1) {
      top: 18px;
    }
    &:nth-child(2) {
      top: 28px;
    }
    &:nth-child(3) {
      top: 38px;
    }
  }
  @media (max-width: 870px) {
    display: block;
  }
  :hover,
  :active {
    background: #eee;
    transform: scale(1.05);
  }
`

const Drawer = styled.aside`
  position: fixed;
  height: 100%;
  width: ${({ isOpen }) => (isOpen ? "250px" : "0")};
  transform: ${({ isOpen }) => (isOpen ? "translateX(0)" : "translateX(50px)")};
  z-index: 300;
  top: 0;
  right: 0;
  background-color: ${({ background }) => (background ? background : "white")};
  color: ${({ color }) => (color ? color : "black")};
  overflow-x: hidden;
  transition: 0.5s;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  box-sizing: border-box;
  padding: 1.5rem;
  text-align: ${({ alignCenter }) => (alignCenter ? "center" : "left")};

  ${({ isOpen }) => isOpen && "box-shadow: 0 0 10px 10px rgba(0, 0, 0, 0.3);"}
  .lang,
  .social {
    display: flex;
    justify-content: space-around;
  }

  a {
    text-decoration: none;
    color: ${({ color }) => (color ? color : "black")};
    &.active {
      text-decoration: underline;
      font-weight: bold;
      pointer-events: none;
    }
  }
`

const FooterHeader = styled.div`
  display: flex;
  justify-content: space-between;
  span:first-child {
    line-height: 20px;
    font-size: 50px;
  }
  span:last-child {
    font-size: 20px;
    align-self: center;
  }
`

const Logo = styled.img`
  height: 60px;
  width: auto;
`

const LinkStyled = styled.a`
  display: block;
  color: ${({ color }) => (color ? color : "black")};
  margin: 2rem 0;
  text-decoration: none;
`

const Hamburger = ({ onClick }) => (
  <Button onClick={onClick}>
    <span />
    <span />
    <span />
  </Button>
)

const MobileMenu = ({ redirect, logo, lang, t }) => {
  const ref = useRef()
  const [isOpen, setIsOpen] = useState(false)
  const [isFooterOpen, setIsFooterOpen] = useState(false)
  function closeAll() {
    setIsFooterOpen(false)
    setIsOpen(false)
  }
  useOnClickOutside(ref, closeAll)
  return (
    <header>
      <Hamburger onClick={() => setIsOpen(true)} />
      <Drawer isOpen={isOpen} ref={ref} alignCenter>
        <Logo src="/logo.svg" alt="Logo" />
        <nav>
          <LinkStyled
            onClick={closeAll}
            href={`${redirect ? "/" : ""}#services`}
          >
            {t("services")}
          </LinkStyled>
          <LinkStyled
            onClick={closeAll}
            href={`${redirect ? "/" : ""}#products`}
          >
            {t("products")}
          </LinkStyled>
          <LinkStyled
            onClick={closeAll}
            href={`${redirect ? "/" : ""}#clients`}
          >
            {t("clients_suppliers")}
          </LinkStyled>
          <LinkStyled
            onClick={closeAll}
            href={`${redirect ? "/" : ""}#contact`}
          >
            {t("contact")}
          </LinkStyled>
        </nav>
        <div className="social">
          <a
            href="https://instagram.com/mayka_decoracion"
            target="_blank"
            rel="noopener noreferrer"
          >
            <Instagram color="#333" width={30} height={30} />
          </a>
          <a
            href="https://www.facebook.com/CortinasMayka/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <Facebook color="#333" width={30} height={30} />
          </a>
        </div>
        <div className="lang">
          <a className={lang === "es" ? "active" : ""} href="/">
            ES
          </a>
          <a className={lang === "en" ? "active" : ""} href="/en">
            EN
          </a>
        </div>
        <div role="button" onClick={() => setIsFooterOpen(true)} tabIndex={0}>
          + INFO
        </div>
        <Drawer
          isOpen={isFooterOpen}
          background="black"
          color="white"
          alignCenter
        >
          <FooterHeader onClick={() => setIsFooterOpen(false)}>
            <span>
              <Back />
            </span>
            <span>+INFO</span>
          </FooterHeader>
          <div>
            <LinkStyled color="white" href="/privacy">
              {t("privacy")}
            </LinkStyled>
            <LinkStyled color="white" href="/legal">
              {t("legal")}
            </LinkStyled>
          </div>
          <div style={{ fontSize: 12 }}>
            <div>
              Made with (L) by{" "}
              <a href="http://www.estudioyobo.com">Estudio Yobo</a>
            </div>
            <div>©2019, Cortinas Mayka</div>
          </div>
        </Drawer>
      </Drawer>
    </header>
  )
}

export default MobileMenu
