import React from "react"
import styled from "styled-components"
import { GatsbyImage } from "gatsby-plugin-image"
import namespace from "../../utils/namespace"

const Section = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  display: grid;
  width: 100%;
  height: 100vh;
  grid-template-columns: 5% 5% auto 5% 50%;
  grid-template-rows: repeat(6, 1fr);
  background: white;

  .title {
    grid-column: 2 / 4;
    grid-row: 2;
    margin: 0;
  }

  .idx {
    grid-column: 2 / 4;
    grid-row: 2;
    align-self: end;
    span {
      font-weight: 300;
      font-size: 18px;
      margin-right: 20px;
      cursor: pointer;
      &.active {
        font-weight: 700;
        font-size: 25px;
      }
    }
  }
  .content {
    grid-column: 3 / 4;
    grid-row: 4 / -1;
    align-items: center;
    h2 {
      text-transform: uppercase;
      font-weight: 400;
    }
  }
  .img {
    grid-column: 5 / 6;
    grid-row: 1 / 7;
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
  &.service_0 {
    z-index: 1;
  }
  &.leave {
    z-index: 1;
  }
  &.service--active {
    z-index: 2;
  }
  @media (max-width: 1200px) {
    .content {
      grid-column: 1 / 5;
      width: 80%;
      margin: 0 auto;
    }
  }
  @media (max-width: 1000px) {
    .content {
      grid-row: 3 / -1;
    }
  }
  @media (max-width: 900px) {
    .content {
      grid-column: 1 / 5;
    }
  }
  @media (max-width: 600px) {
    .content {
      grid-column: 2 / 8;
      grid-row: 2;
    }
    .img {
      grid-column: 1 / 9;
      grid-row: 4 / 7;
      width: 100%;
      height: 100%;
    }
    .title {
      grid-column: 2 / 8;
      grid-row: 1;
      align-self: center;
    }
    .idx {
      grid-column: 2 / 8;
      grid-row: 1;
      align-self: center;
      justify-self: end;
      span {
        display: none;
        &.active {
          display: inline;
        }
      }
    }
  }
`

const Indexes = ({ current, sections }) => {
  let a = []
  for (let i = 0; i < sections; i++) {
    a.push(
      <span
        key={i}
        role="button"
        tabIndex={0}
        className={current === i ? "active" : ""}
        onClick={() => {
          window.scroll({
            left: 0,
            top: window.innerHeight + 400 * (i + 1),
          })
        }}
      >
        {`${i + 1}`.padStart(2, "0")}
      </span>
    )
  }
  return a
}

const Service = ({
  className,
  title,
  description,
  image,
  index,
  sections,
  lang,
}) => {
  console.log(image)
  const t = namespace(lang)
  return (
    <Section className={className}>
      <h1 className="title">{t("services")}</h1>
      <h2 className="idx">
        <Indexes current={index} sections={sections} />
      </h2>
      <div className="content">
        <h2>{title[lang]}</h2>
        <p>{description[lang]}</p>
      </div>
      <GatsbyImage image={image.asset.gatsbyImageData} alt="" className="img" />
    </Section>
  )
}

export default Service
