import React, { useEffect } from "react"
import Service from "./Service"
import { TweenLite, TimelineLite, gsap } from "gsap"
import ScrollMagic from "scrollmagic"
import styled from "styled-components"

import initPlugin from "./gsap-scrollmagic.plugin"

const Section = styled.section`
  position: relative;
  height: 100vh;
`

const DURATION = 300

const Services = ({ services, lang }) => {
  useEffect(() => {
    initPlugin(ScrollMagic, TweenLite, TimelineLite)
    const controller = new ScrollMagic.Controller({
      globalSceneOptions: {
        triggerHook: "onLeave",
      },
    })

    new ScrollMagic.Scene({
      duration: DURATION * services.length,
      triggerElement: "#services",
    })
      .setPin("#services")
      .addTo(controller)
    services.forEach((service, i) => {
      // className: "+=service--active"
      var tween = gsap.to(`.service_${i}`, 0.3, {
        zIndex: 1,
      })
      new ScrollMagic.Scene({
        duration: DURATION,
        offset: DURATION * i,
        triggerElement: "#services",
      })
        .setTween(tween)
        .addTo(controller)
    })
  }, [services])
  return (
    <Section id="services">
      {services.map((service, i) => (
        <Service
          className={`service_${i}`}
          key={i}
          index={i}
          sections={services.length}
          lang={lang}
          {...service}
        />
      ))}
    </Section>
  )
}

export default Services
